#!/usr/bin/env python3
# encoding: utf-8
import socket, argparse, json, logging, threading, sys, time, re, random, xkcd, queue
from slackclient import SlackClient
from pymarkovchain import MarkovChain

LOGLEVEL_RECV = 18
LOGLEVEL_SENT = 17

logging.addLevelName(LOGLEVEL_RECV, "RECV")
logging.addLevelName(LOGLEVEL_SENT, "SENT")
logging.basicConfig(level=15, filename="haley.log", format="%(asctime)s %(levelname)s %(message)s")

def detox(s):
    return re.sub("[^a-zA-Z0-9\s]", "", s)

class Backend(object):
    def connect(self):
        raise NotImplementedError
    def update(self):
        """Main loop. Returns a list of new happenings."""
        raise NotImplementedError
    def say(self, message):
        raise NotImplementedError
    def act(self, action):
        raise NotImplementedError
    def get_name(self, nickname):
        raise NotImplementedError

class IRCBackend(Backend):
    def __init__(self, host, port, channel, nickname):
        self.host = host
        self.port = port
        if "#" not in channel:
            self.channel = "#" + channel
        else:
            self.channel = channel
        self.nickname = nickname
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    def connect(self):
        self.socket.connect((self.host, self.port))
        self.buffer = ""
    def update(self):
        self.buffer += self.socket.recv(512).replace("\r", "")
        ret = []
        while "\n" in buff:
            message, buff = buff.split("\n", 1)
            logging.log(LOGLEVEL_RECV, message)
            if "Found your" in message or "checking ident" in message:
                self.socket.sendall("USER %s %s s: %s" % (self.nickname, self.nickname, self.nickname))
                self.socket.sendall("NICK %s" % self.nickname)
                continue
            if "nickname is already in use" in message.lower():
                self.nickname += "_"
                self.socket.sendall("NICK %s" % self.nickname)
                continue
            if message.upper().startswith("PING"):
                self.socket.sendall("PONG%s" % message[4:])
                continue
            if message.startswith(":") and ("001 %s :" % self.nickname) in message and "PRIVMSG" not in message.upper():
                self.socket.sendall("JOIN %s" % self.channel)
                continue
            if (" PRIVMSG %s :" % self.channel) in message:
                friend = message.split(":")[1].split(" ")[0].split("!")[0]
                text = message.split(" PRIVMSG %s :" % self.channel, 1)[1]
                logging.log(LOGLEVEL_RECV, "%s: %s", friend, text)
                if text.startswith("ACTION "):
                    ret.append({"type": "action", "by": friend, "content": text, "mentioned": (self.nickname in text)})
                else:
                    ret.append({"type": "text", "by": friend, "content": text, "mentioned": (self.nickname in text)})
                continue
        return ret
    def say(self, message):
        for line in message.split("\n"):
            logging.log(LOGLEVEL_SENT, "%s", line)
            self.socket.sendall("PRIVMSG %s :%s" % (self.channel, line))
    def act(self, action):
        for line in message.split("\n"):
            logging.log(LOGLEVEL_SENT, "/me %s", line)
            self.socket.sendall("PRIVMSG %s :\x01ACTION %s\x01" % (self.channel, line))
    def get_name(self, nickname):
        return nickname

class SlackBackend(Backend):
    def __init__(self, token, channel=None):
        self.token = token
        self.channel = channel
        self.queue = queue.Queue()
        self.last_sent = 0
    def connect(self):
        self.slack = SlackClient(self.token)
        self.slack.rtm_connect()
        self.uid = json.loads(self.slack.api_call("auth.test").decode())["user_id"]
        logging.info(self.uid)
    def update(self):
        ret = []
        for event in self.slack.rtm_read():
            if event.get("type", "") == "message":
                self.channel = self.channel or event.get("channel","#general")
                mentioned = self.uid in event.get("text","") or self.slack.server.username.lower() in event.get("text","").lower()
                logging.log(LOGLEVEL_RECV, "%s%s %s", event.get("user", ""), "" if mentioned else ":", event.get("text",""))
                ret.append({"type": "action" if event.get("subtype", "") == "me_message" else "text", "by": "<@%s>"%event.get("user", ""), "content": event.get("text",""), "mentioned": mentioned})
        while time.time()-self.last_sent > 2 and not self.queue.empty():
            message = self.queue.get(block=False)
            logging.log(LOGLEVEL_SENT, "%s", message)
            self.slack.server.channels.find(self.channel).send_message(message)
            self.last_sent = time.time()
        return ret
    def say(self, message):
        self.queue.put(message)
    def act(self, message):
        logging.log(LOGLEVEL_SENT, "/me %s", message)
        self.slack.server.channels.find(self.channel).send_message("_%s_"%message)
    def get_name(self, nickname):
        p = json.loads(self.slack.api_call("users.info", user=re.sub(r"[>@<]","", nickname)).decode())
        n = p.get("user", {}).get("profile", {})
        n = n.get("real_name", "")
        if n:
            return n
        else:
            return nickname

class Haley(object):
    def __init__(self, backend):
        self.backend = backend
        self.mc = MarkovChain("markov.db")
    def loop(self):
        self.backend.connect()
        while True:
            for event in self.backend.update():
                try:
                    if event["type"] == "text":
                        times = re.search(r"(?P<nm>\d+) times", event["content"].lower())
                        if times:
                            if int(times.group("nm")) > 0:
                                times = min(5,int(times.group("nm")))
                            else:
                                self.backend.say("Okay, I won't say anything... Baka.")
                                continue
                        else:
                            times = 1
                        for i in range(times):
                            if "hi" in detox(event["content"].lower()).split() or "hello" in detox(event["content"].lower()).split():
                                self.backend.say(random.choice(["%s! Tutturuuu!","Hello, %s, so it was you making the noise up there!"]) % event["by"])
                                continue
                            if "nano" in event["content"].lower() or "hakase" in event["content"].lower():
                                self.backend.say("%s%s"%("HAKASE"*len(re.findall("nano", event["content"].lower())),"NANO"*len(re.findall("hakase", event["content"].lower()))))
                                continue
                            if event["mentioned"]:
                                if "roll" in detox(event["content"].lower()).split():
                                    numb = re.search(r"(d|k)(?P<nm>\d+)", event["content"].lower())
                                    if numb and int(numb.group("nm")) > 0:
                                        self.backend.say("Aaaand... %d!" % (random.randrange(1,int(numb.group("nm"))+1)))
                                        continue
                                    else:
                                        self.backend.say("Who do you think you are, rolling impossible dice... Baka.")
                                        continue
                                if "say" in detox(event["content"].lower()).split():
                                    if "something" in detox(event["content"].lower()).split():
                                        tosay = self.mc.generateString()
                                    elif "name" in detox(event["content"].lower()).split():
                                        tosay = self.backend.get_name(event["by"])
                                    self.backend.say(tosay)
                                    continue
                                if "xkcd" in detox(event["content"].lower()).split():
                                    if "random" in detox(event["content"].lower()).split():
                                        x = xkcd.getRandomComic()
                                    else:
                                        numb = re.search(r"(?P<nm>\d+)", event["content"])
                                        if numb:
                                            x = xkcd.Comic(int(numb.group("nm")))
                                        else:
                                            x = xkcd.getLatestComic()
                                    self.backend.say("*%s* - %s - _%s_" % (x.getTitle(), x.getImageLink(), x.getAltText()))
                                    continue
                                self.backend.say("Hmm?")
                                continue
                except:
                    self.backend.say(str(sys.exc_info()[0]))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    backends = parser.add_subparsers(help="Backend to use")
    backend_irc = backends.add_parser("irc")
    backend_irc.add_argument("-s", "--server", type=str, default="irc.pirc.pl")
    backend_irc.add_argument("-p", "--port", type=int, default=6667)
    backend_irc.add_argument("-n", "--nickname", type=str, default="haley")
    backend_irc.add_argument("-c", "--channel", type=str, default="#futuregadgetlab")
    backend_irc.set_defaults(func=lambda args: IRCBackend(args.server, args.port, args.channel, args.nickname))
    backend_slack = backends.add_parser("slack")
    backend_slack.add_argument("token", type=str)
    backend_slack.set_defaults(func=lambda args: SlackBackend(args.token))
    args = parser.parse_args()
    if "func" in args:
        Haley(args.func(args)).loop()
    else:
        parser.parse_args(["-h"])
